# async_worker

## external dependencies
- running kafka 
- BROKER_URL environment variable

## how to run worker instance
```
faust -A app worker -l info
```
## Build and Running <h4>

How to test this?

Install Docker Compose
Clone this repository
Change in docker-compose file variable: BROKER_URL to ip your kafka host ex: kafka://192.168.1.10:9022
Run all containers with docker-compose build & docker-compose up -d
Verify in your kafka connection application async_worker
