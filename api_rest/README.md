# api_rest

## how to run api server
```
uvicorn app:app --port 8000
```
## Build and Running <h4>

How to test this?

Install Docker Compose
Clone this repository
Add in your host entry dns app.virtualti.xyz point to ip local ex 127.0.0.1 
Run all containers with docker-compose build & docker-compose up -d
Verify api_rest that your container running in port 8000 access https://app.virtualti.xyz:8000/health
