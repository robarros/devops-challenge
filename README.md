# devops-challenge

## Versioning Pattern

This project uses GitFlow as git versioning pattern.

## Installing Dependencies

```
# change dir to service dir
poetry shell
poetry install
```
A sample application to demonstrate continuous integration in Gitlab.

## Code

The project content is based on the two application api_rest and async_worker.
In  each folder has file .gitlab.yml with the steps that will be performed Scan, Build, Deploy.
The deploy is performed by the helm.

In the Scan step, the system will fetch the last TAG from the repository that will be used in the docker image.
In the second step, the build of the image made by Kaniko will be executed and it will be automatically sent to the DockerHub repository.
In the third step, the applications will be deployed by the helm.
will be deployed in the development and production environment.

To work correctly you need to add 2 variables in GitLab:
DOCKERCONFIG = base64 username and password:
```
echo -n USER:PASSWOD | base64
```

copy the result to this variable.

K8S = kubeconfig to access the kubernetes cluster
create this variable and add the kubeconfi content to it.
